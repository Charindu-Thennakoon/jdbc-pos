# POS System With JDBC

POS System using Java and MySQL.

In this **POS System** has all the functionalities of a POS system, like customer adding,item management,order placing,order viewing and order searching.It consists of five main parts;

    * Simple use login management
    * Manage customers
    * Manage items
    * Placeorders
    * View orders
    * Search orders

## Libraries

* [JFoenix](http://www.jfoenix.com/) - is an open source Java library, that implements Google Material Design using Java components.
* [Font Awsome](https://jar-download.com/artifacts/de.jensd/fontawesomefx/8.2/source-code)
*[Font Awsome](https://github.com/Jerady/fontawesomefx-glyphsbrowser) -Easy icon library please download 8.2 vesion,higher or latter varsions do not work 
* [MySQL Connector](https://dev.mysql.com/downloads/connector/j/) - MySQL Connector/J is the official JDBC driver for MySQL.


## Database

Find Database Configuration From App/src/dbConnection and set your Database name,user name and password.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)